# Project e-commerce : Tiny Tiny House

## contexte du projet
---- 
Lors de ce projet notre équipe à réaliser un site de vente en ligne.
L'équipe à fait un brainstorming pour trouver un thème innovant. Nous avons choisie les tiny house car c'est un produit en vogue. Cet un habitant minimaliste et écologique qui ce veut plus proche de la nature. 

![MoodBoard canva ](public/assets/imgRead/couleurs.png)

Nos contraintes de travail sont les suivantes:

## Conception:
----
#### Réalisation de diagrammes de Use Case:

![Usecase](public/assets/imgRead/usercase-eshop.png)

#### Réalisation d'un diagramme de classe:

![Diagramme de classe](public/assets/imgRead/diagrammedeClasse.png)

#### Réalisation de maquettes (wireframe) mobile first:

![Wireframe mobile first](public/assets/imgRead/maquette%20mobile-first.png)

## Technique
----
#### Utilisation Express.js et TypeORM pour le back:

. Express est une infrastructure d'applications Web Node.js minimaliste et flexible qui fournit un ensemble de fonctionnalités robuste pour les applications Web et mobiles.
```npm install express --save```

![Express](public/assets/imgRead/express.png)


. Typeorm est comme tout autre ORM, il permet d'interagir avec plusieurs bases de données et prend en charge plusieurs pilotes de base de données pour typeorm, il prend en charge MySQL, MariaDB, PostgreSQL, CockroachDB, SQLite, Microsoft SQL Server sql.js Oracle et MongoDB.
Typeorm est livré avec une CLI robuste qui nous permet de gérer nos projets et d'interagir avec la base de données.

```npm install typeorm -g```

```typeorm init --name Typeorm-project --database mysql```

. Entités Typeorm

Entity est une classe qui représente une table de base de données, les champs de la classe sont les colonnes de la table

![Entity Typeorm](public/assets/imgRead/entityOrm.png)

![Creer une connnection avec Typeorm](public/assets/imgRead/creatConnectionTypeorm.png)

----

#### Utilisation de React pour le front (avec Typescript et pourquoi pas Redux/RTK Query)

```npx create-react-app my-app --template redux-typescript```


Application responsive:
Nous avons utiliser la Librairie Antd, afin de stylisé et de rendre responsive notre projet.
Antd fonctionne avec un systeme de colonne et de ligne.

## Organisation Agile
----
#### Utilisation de git/gitlab pour le travail en groupe

User stories:

![Ajouter un produit](public/assets/imgRead/addproduct.png)
![Register](public/assets/imgRead/register.png)
![Login](public/assets/imgRead/login.png)
![Chercher un produit](public/assets/imgRead/productSearch.png) features à venir


Nous avons organiser des sprints à l'aide de la partie Issues et du Board du projet gitlab

![gitlab](public/assets/imgRead/ticketsSprint.png)

Au début de chaque sprint,nous avons défini le livrable attendu, les tâches à réaliser.
En début de chaque après midi, nous avons fait un daily scrum de 15 minutes max pour échanger sur nos soucis
A la fin de chaque sprint, nous avons fait une review.