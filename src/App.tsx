import { BrowserRouter, Route, Switch } from 'react-router-dom';
import './App.css';
import { AdminPage } from './pages/AdminPage';
import { Nav } from './components/Nav';
import Foot from './components/Foot';
import { LoginPage } from './pages/LoginPage';
import { Home } from './pages/Home';
//import { UserProfile } from './pages/UserProfile';
import { Layout } from 'antd';
import { Content, Header, Footer } from 'antd/lib/layout/layout';
import { UserPage } from './pages/UserPage';
import { RegistrationForm } from './components/RegisterForm';
import { ProductDetail } from './pages/ProductDetail';
import { useGetThisUserQuery } from './utils/userApi';
import { Cart } from './pages/Cart';


export interface RouterParams {
    id:string
}


function App() {
    const token = useGetThisUserQuery(undefined, { skip: localStorage.getItem('token') === null })
    if (token.isError) {
        localStorage.removeItem('token')
    }
    return (

        <BrowserRouter>
            <Layout>
                <Header>
                    <Nav />
                </Header>

                <Layout >
                    <Content>
                        <Switch>

                            <Route path="/" exact>
                                <Home />
                            </Route >
                            <Route path="/AdminPage">
                                <AdminPage />
                            </Route>
                            <Route path="/Cart">
                                <Cart />
                            </Route>

                            <Route path="/login">
                                <LoginPage />
                            </Route>

                            <Route path="/product/:id">
                                <ProductDetail/>
                            </Route>


                            <Route path="/register">
                                <RegistrationForm />
                            </Route>

                            <Route path="/user">
                                <UserPage />
                            </Route>

                        </Switch>
                    </Content>
                </Layout >
                <footer>
                    <Foot />
                </footer>
            </Layout>
        </BrowserRouter>
    );
}

export default App;