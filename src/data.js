export const header = [
    {
        title: '产品',
        children: [
            {
                title: '云凤蝶', desc: '移动建站平台', img: 'https://gw.alipayobjects.com/zos/rmsportal/fLPzRmwAurHkPDVfHHiQ.svg', link: 'https://fengdie.alipay-eco.com/intro', top: '2px',
            },
        ],
    },
    {
        title: '设计体系',
        children: [
            {
                title: '设计价值观', desc: 'Design Values', img: 'https://gw.alipayobjects.com/zos/rmsportal/zMeJnhxAtpXPZAUhUKJH.svg', link: 'https://ant.design/docs/spec/values-cn',
            },
            {
                title: '视觉', desc: 'Visual', img: 'https://gw.alipayobjects.com/zos/rmsportal/qkNZxQRDqvFJscXVDmKp.svg', link: 'https://ant.design/docs/spec/colors-cn',
            },
            {
                title: '可视化', desc: 'Visualisation', img: 'https://gw.alipayobjects.com/zos/rmsportal/MrUQjZNOJhYJCSZZuJDr.svg', link: 'https://antv.alipay.com/zh-cn/vis/index.html',
            },
        ],
    },
    {
        title: '技术方案',
        children: [
            {
                title: 'Ant Design', desc: '蚂蚁 UI 体系', img: 'https://gw.alipayobjects.com/zos/rmsportal/ruHbkzzMKShUpDYMEmHM.svg', link: 'https://ant.design',
            },
            {
                title: 'AntV', desc: '蚂蚁数据可视化解决方案', img: 'https://gw.alipayobjects.com/zos/rmsportal/crqUoMinEgjMeGGFAKzG.svg', link: 'https://antv.alipay.com',
            },
            {
                title: 'Egg', desc: '企业级 Node 开发框架', img: 'https://gw.alipayobjects.com/zos/rmsportal/nEEwwpmNVihZimnBAtMf.svg', link: 'https://eggjs.org',
            },
        ],
    },
    {
        title: '关于',
        children: [
            {
                title: '蚂蚁金服体验科技专栏', desc: '探索极致用户体验与最佳工程实践', img: 'https://gw.alipayobjects.com/zos/rmsportal/VsVqfjYxPTJaFbPcZqMb.svg', link: 'https://zhuanlan.zhihu.com/xtech',
            },
        ],
    },
];
export const banner = [
    {
        img: 'https://gw.alipayobjects.com/zos/rmsportal/cTyLQiaRrpzxFAuWwoDQ.svg',
        imgMobile: 'https://gw.alipayobjects.com/zos/rmsportal/ksMYqrCyhwQNdBKReFIU.svg',
        className: 'seeconf-wrap',
        children: [
            { children: 'Seeking Experience & Engineering Conference', className: 'seeconf-en-name' },
            { children: 'exemple 1', className: 'seeconf-title', tag: 'h1' },
            { children: 'exemple 1', className: 'seeconf-cn-name' },
            {
                children: '了解详细',
                className: 'banner-button',
                tag: 'button',
                link: 'https://seeconf.alipay.com/',
            },
            { children: '2018.01.06 / 中国·杭州', className: 'seeconf-time' },
        ],
    },
    {
        img: 'https://gw.alipayobjects.com/zos/rmsportal/cTyLQiaRrpzxFAuWwoDQ.svg',
        imgMobile: 'https://gw.alipayobjects.com/zos/rmsportal/ksMYqrCyhwQNdBKReFIU.svg',
        className: 'seeconf-wrap',
        children: [
            { children: 'Seeking Experience & Engineering Conference', className: 'seeconf-en-name' },
            { children: 'exemple 2', className: 'seeconf-title', tag: 'h1' },
            { children: 'exemple 2', className: 'seeconf-cn-name' },
            {
                children: '了解详细',
                className: 'banner-button',
                tag: 'button',
                link: 'https://seeconf.alipay.com/',
            },
            { children: '2018.01.06 / 中国·杭州', className: 'seeconf-time' },
        ],
    },
];
export const page1 = {
    title: 'Our Tiny Houses ',
    children: [
        {
            title: 'High quality',
            content: 'Best trees',
            src: 'https://gw.alipayobjects.com/zos/rmsportal/KtRzkMmxBuWCVjPbBgRY.svg',
            color: '#EB2F96',
            shadowColor: 'rgba(166, 55, 112, 0.08)',
            link: 'https://gitlab.com/projet-eshop/front-eshop',
        },
        {
            title: 'Cheap Price',
            content: 'Our woods satisfy clients needs',
            src: 'https://gw.alipayobjects.com/zos/rmsportal/qIcZMXoztWjrnxzCNTHv.svg',
            color: '#1890FF',
            shadowColor: 'rgba(15, 93, 166, 0.08)',
            link: 'https://gitlab.com/projet-eshop/front-eshop',
        },
        {
            title: 'Various Design',
            content: 'Different styles for different experiences',
            src: 'https://gw.alipayobjects.com/zos/rmsportal/eLtHtrKjXfabZfRchvVT.svg',
            color: '#AB33F7',
            shadowColor: 'rgba(112, 73, 166, 0.08)',
            link: 'https://gitlab.com/projet-eshop/front-eshop',
        },
    ],
};

export const page3 = {
    title: 'Page 3',
    children: [
        {
            img: 'https://gw.alipayobjects.com/zos/rmsportal/iVOzVyhyQkQDhRsuyBXC.svg',
            imgMobile: 'https://gw.alipayobjects.com/zos/rmsportal/HxEfljPlykWElfhidpxR.svg',
            src: 'https://gw.alipayobjects.com/os/rmsportal/gCFHQneMNZMMYEdlHxqK.mp4',
        },
        {
            img: 'https://gw.alipayobjects.com/zos/rmsportal/iVOzVyhyQkQDhRsuyBXC.svg',
            imgMobile: 'https://gw.alipayobjects.com/zos/rmsportal/HxEfljPlykWElfhidpxR.svg',
            src: 'https://gw.alipayobjects.com/os/rmsportal/gCFHQneMNZMMYEdlHxqK.mp4',
        },
    ],
};

export const page4 = {
    title: 'Page 4',
    children: [
        'https://gw.alipayobjects.com/zos/rmsportal/qImQXNUdQgqAKpPgzxyK.svg', // 阿里巴巴
        'https://gw.alipayobjects.com/zos/rmsportal/LqRoouplkwgeOVjFBIRp.svg', // 蚂蚁金服
        'https://gw.alipayobjects.com/zos/rmsportal/TLCyoAagnCGXUlbsMTWq.svg', // 人民网
        'https://gw.alipayobjects.com/zos/rmsportal/HmCGMKcJQMwfPLNCIhOH.svg', // cisco
        'https://gw.alipayobjects.com/zos/rmsportal/aqldfFDDqRVFRxqLUZOk.svg', // GrowingIO
        'https://gw.alipayobjects.com/zos/rmsportal/rqNeEFCGFuwiDKHaVaPp.svg', // 饿了么
        'https://gw.alipayobjects.com/zos/rmsportal/FdborlfwBxkWIqKbgRtq.svg', // 滴滴出行
        'https://gw.alipayobjects.com/zos/rmsportal/coPmiBkAGVTuTNFVRUcg.png', // 飞凡网
    ],
};

export const footer = [
    {
        title: 'TinyTiny House',
        children: [
            { title: 'Products build in Europe', link: 'https://gitlab.com/projet-eshop/front-eshop' },
            { title: 'Over 200 workers', link: 'https://gitlab.com/projet-eshop/front-eshop' },
            { title: '10 millions of dollars earned per year', link: 'https://gitlab.com/projet-eshop/front-eshop' },
        ],
    },
    {
        title: 'Companies that work with us',
        children: [
            { title: 'TinyTiny House Junior', link: 'https://gitlab.com/projet-eshop/front-eshop' },
            { title: 'International Tiny House', link: 'https://gitlab.com/projet-eshop/front-eshop' },
        ],
    },
    {
        title: 'Other informations',
        children: [
            { title: 'CEO Located in Lyon, France', link: 'https://zhuanlan.zhihu.com/xtech' },
            { title: 'Wood constructor located in Germany', link: 'https://weibo.com/p/1005056420205486' },
            { title: 'Painting from Russia', link: '' },
            { title: 'Architect from the UK', link: '' },
        ],
    },
    {
        title: 'Get in touch',
        icon: 'https://gw.alipayobjects.com/zos/rmsportal/wdarlDDcdCaVoCprCRwB.svg',
        children: [
            { title: 'Email', desc: 'tiny@tiny.com', link: 'https://gitlab.com/projet-eshop/front-eshop' },
            { title: 'Phone number ', desc: '+848328778237823', link: 'https://gitlab.com/projet-eshop/front-eshop' },
            // { title: 'AntG', desc: '蚂蚁互动图形技术', link: 'http://antg.alipay.net' },
            { title: 'Address', desc: '34 rue du Sadena', link: 'https://gitlab.com/projet-eshop/front-eshop' },
            { title: 'Twitter', desc: '@TinyTinyHouse', link: 'https://gitlab.com/projet-eshop/front-eshop' },
        ],
    },
];

// 图处预加载；
if (typeof document !== 'undefined') {
    const div = document.createElement('div');
    div.style.display = 'none';
    document.body.appendChild(div);
    [
        'https://gw.alipayobjects.com/zos/rmsportal/KtRzkMmxBuWCVjPbBgRY.svg',
        'https://gw.alipayobjects.com/zos/rmsportal/qIcZMXoztWjrnxzCNTHv.svg',
        'https://gw.alipayobjects.com/zos/rmsportal/eLtHtrKjXfabZfRchvVT.svg',
        'https://gw.alipayobjects.com/zos/rmsportal/iVOzVyhyQkQDhRsuyBXC.svg',
        'https://gw.alipayobjects.com/zos/rmsportal/HxEfljPlykWElfhidpxR.svg',
        'https://gw.alipayobjects.com/zos/rmsportal/wdarlDDcdCaVoCprCRwB.svg',
    ].concat(page4.children).forEach((src) => {
        const img = new Image();
        img.src = src;
        div.appendChild(img);
    });
}
