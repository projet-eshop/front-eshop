import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { AuthState } from '../app/authSlice';
import { User } from '../entities';
import { prepare } from './token';

export const userApi = createApi({

    reducerPath: 'userApi',
    tagTypes: ['user'],
    baseQuery: fetchBaseQuery({ baseUrl: process.env.REACT_APP_SERVER_URL + '/api/user', prepareHeaders: prepare }),
    endpoints: (builder) => ({

        getThisUser: builder.query<User, void>({
            query: () => '/account',
            providesTags: ['user']
        }),
        
        userLogin: builder.mutation<AuthState, User>({
            query: (body) => ({
                url: '/login',
                method: 'POST',
                body
            }),
            invalidatesTags: ['user']
        }),
        userRegister: builder.mutation<User, User>({
            query: (body) => ({
                url: '/register',
                method: 'POST',
                body, 
            }),
     

            invalidatesTags: ['user']
        }), 
        

        addToCart: builder.mutation<User, number>({
            query: (id) => ({
                url: '/cart/'+id,
                method: 'POST'
            }),
            invalidatesTags: ['user']
        })
    })
});

export const { useGetThisUserQuery, useUserLoginMutation, useUserRegisterMutation, useAddToCartMutation } = userApi
