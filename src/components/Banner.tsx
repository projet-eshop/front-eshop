import { Row, Col } from 'antd';
import './Banner.css'

export default function Banner () {

    return (


        <div >
            <div className="littleImages" >
                <h1>Why TinyTinyHouse ?</h1>
                <i />
                <Row className="rowBanner" >
                    <Col  >
                        <div className="imagesBanner">
                            <img src={'https://i.ibb.co/NFLMLvF/tree.png'} alt="fireSpot" />
                            <img src={'https://i.ibb.co/ggZg6Hm/hand.png'} alt="fireSpot" />
                            <img src={'https://i.ibb.co/Bsv2MqJ/home-icon-silhouette.png'} alt="fireSpot" />
                        </div>
                    </Col>
                </Row>
            </div>
        </div>);
}

