import { Product } from "../entities";
import 'antd/dist/antd.css';
import { Carousel, Card, Col, Row } from 'antd';
import { useDeleteProductMutation} from "../app/AdminProductApi";
import { EditOutlined, CommentOutlined, DeleteOutlined } from '@ant-design/icons';
import '../pages/Admin.css'

const { Meta } = Card;


const contentStyle = {
    height: '160px',
    color: '#fff',
    lineHeight: '160px',
    textAlign: 'center',
    background: '#364d79',
};


interface Props {
    product: Product;
}

export function AdminProduct({ product }: Props) {

    const [deleteProduct, { isSuccess }] = useDeleteProductMutation();
    const [updateProduct, setUpdatingProduct] = useDeleteProductMutation();


    const onFinish = async (id: any) => {

        await deleteProduct(id);

    };




    return (

        <Row>
            <Col>
        <Card
            style={{ width: 300 }}


            actions={[
                <CommentOutlined key="comment" />,
                <EditOutlined key="edit" />,
                <DeleteOutlined key="delete"
                    onClick={() => {
                        onFinish(product.id);
                    }}

                />,
            ]}
        >
            <Carousel autoplay>
                {product.images?.map(image =>
                    //product.images[1]
                    <div className="adminCardImage">

                        <img key={image.id} src={image.fileName?.startsWith('http') ? image.fileName : `${process.env.REACT_APP_SERVER_URL}/uploads/${image.fileName}`} />
                        <p>{image.fileName}</p>

                        {/* src={image.fileName?.startsWith('http')?.map(allImage => allImage:process.env.REACT_APP_SERVER_URL + allImage} /> */}

                        {/*                                          <img src={product.images?.startsWith('http')?.map(image => product.images:process.env.REACT_APP_SERVER_URL + image.filename)} />
 */}
                    </div>
                )}


            </Carousel>




        </Card>
        </Col>
        </Row>
    );
}
