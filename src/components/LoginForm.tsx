import { Form, Input, Button, Card } from 'antd';
import { Redirect } from 'react-router-dom';
import { setCredentials } from '../app/authSlice';
import { useAppDispatch, useAppSelector } from '../app/hooks';
import { useUserLoginMutation } from '../utils/userApi';
import './Form.css'


const formItemLayout = {
    labelCol: {
        xs: { span: 24 },
        sm: { span: 2 },
    },
    wrapperCol: {
        xs: { span: 24 },
        sm: { span: 10 },
    },
};


export const LoginForm = () => {
    const [form] = Form.useForm();

    const [loginPost, postQuery] = useUserLoginMutation();
    const user = useAppSelector(state => state.auth.user)

    let dispatch = useAppDispatch()

    const onFinish = async (values: any) => {
        const data = await loginPost(values).unwrap()
        if (data && !postQuery.isError) {
            dispatch(setCredentials(data))
        }

    }
    return (
<div className="loginFormContainer">
        <Card {...formItemLayout} className='card-login' >
            {!user ?

                <Form  className="form" form={form}
                    name="login"
                    wrapperCol={{xs:24, sm:12}}
                    labelCol={{xs:24, sm:6}}
                    onFinish={onFinish}
                    //            initialValues={}
                    scrollToFirstError
                >
                    <Form.Item
                        name="email"
                        label="E-mail"
                        rules={[
                            {
                                type: 'email',
                                message: 'The input is not valid E-mail!',
                            },
                            {
                                required: true,
                                message: 'Please input your E-mail!',
                            },
                        ]}
                    >
                        <Input />
                    </Form.Item>

                    <Form.Item
                        name="password"
                        label="Password"
                        rules={[
                            {
                                required: true,
                                message: 'Please input your password!',
                            }, {
                                min: 8,
                                message: 'Password must have a minimum length of 8'
                            },
                        ]}
                        hasFeedback
                    >
                        <Input.Password />
                    </Form.Item>

                  <div className='button-form'>
                        <Button className="btn-form" type="primary" htmlType="submit" >
                            Login
                        </Button>
                        </div>
                </Form> : <Redirect to='/' />}
        </Card>
        </div>
    );
};