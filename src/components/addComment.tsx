import { Avatar, Form, Button, List, Input, Card } from 'antd';
import moment from 'moment';
import { commentApi, useGetOneCommentQuery, usePostCommentMutation } from '../app/commentApi';
import { Comment } from '../entities';
const { TextArea } = Input;

// const CommentList = ({ comments }) => (
//   <List
//     dataSource={comments}
//     header={`${comments.length} ${comments.length > 1 ? 'replies' : 'reply'}`}
//     itemLayout="horizontal"
//     renderItem={props => <Comment {...props} />}
//   />
// );

interface Props {
    id: number;
    refetch:Function
}

export function TextAreaComment({id, refetch}:Props) {

    const [addComment, setAddComment] = usePostCommentMutation();
    


    const onFinish = async (values: any) => {

        await addComment({body: values, id:id})
        refetch()
    }

    return (
        <div>
            <Form onFinish={onFinish}>
                <Form.Item name="description">
                    <TextArea rows={4} />
                </Form.Item>
                <Form.Item>
                    <Button htmlType="submit" type="primary">
                        Add Comment
                    </Button>
                </Form.Item>
            </Form>





        </div>
    )
};

/*a essayer d'enlever : 
refetch:Function
refetch
refetch()*/