import { Menu } from "antd";
import { Link, useLocation } from "react-router-dom";
import './Nav.css'
import { HomeOutlined, MenuOutlined, ShoppingCartOutlined, UserDeleteOutlined, UserOutlined } from '@ant-design/icons';
import { useAppSelector } from "../app/hooks";
import { logout } from "../app/authSlice";
import { useDispatch } from "react-redux";



export function Nav() {
   
    const location = useLocation();
    const user = useAppSelector(state => state.auth.user);
    const dispatch = useDispatch();

    return (
        <>
            <Menu mode="horizontal" theme="light" selectedKeys={[location.pathname]} style={{ lineHeight: '64px' }} overflowedIndicator={<MenuOutlined />}>

                <Menu.Item className="modified-item" key="/">
                    <Link to="/"><HomeOutlined style={{ fontSize: '30px', color: 'back' }} />Home</Link>
                </Menu.Item>

                {!user ? <>
                    <Menu.Item className="modified-item" key="/login">
                        <Link to="/login"><UserOutlined style={{ fontSize: '30px', color: 'back' }} />Connection </Link>
                    </Menu.Item>

                    <Menu.Item className="modified-item" key="/register">
                        <Link to="/register"><UserOutlined style={{ fontSize: '30px', color: 'back' }} />Register </Link>
                    </Menu.Item>
                </>
                    :
                    <>
                        <Menu.Item className="modified-item" key="/Cart" icon={<ShoppingCartOutlined style={{ fontSize: '30px', color: 'back' }} />}>
                            <Link to="/Cart"></Link>
                        </Menu.Item>

                        <Menu.Item className="modified-item" key="/Cart" icon={<UserDeleteOutlined style={{ fontSize: '30px', color: 'back' }} />}>
                            <Link onClick={() => dispatch(logout())} to="/"></Link>
                        </Menu.Item>
                    </>
                }
                {user?.role === 'admin' ? <>
                    <Menu.Item className="modified-item" key="/AdminPage">
                        <Link to="/AdminPage">Admin</Link>
                    </Menu.Item>

                </> : null
                }
            </Menu>
        </>
    )
}
