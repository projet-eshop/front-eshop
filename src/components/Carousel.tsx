import { Carousel, Col, Image, Row } from 'antd';
import { AlignType } from 'rc-table/lib/interface'
import { useGetAllProductsQuery } from '../app/AdminProductApi';
import './carousel.css'

export function CarouselComponent() {
    const contentStyle = {
        height: '35em',
        color: '#fff',
        lineHeight: '160px',
        textAlign: 'center' as AlignType,
        background: '#364d79',
    };

    const { data } = useGetAllProductsQuery()

    return (


        <div className="carouselImage">
            <img src="https://i.ibb.co/8xrpT2t/photo-1564013434775-f71db0030976.jpg" />
        </div>
    )

}