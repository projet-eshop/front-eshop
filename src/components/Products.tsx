import { Card, Image, Row } from 'antd';
import Text from 'antd/lib/typography/Text';
import { useGetAllProductsQuery } from '../app/AdminProductApi';
import { Product } from '../entities';
import { Button } from 'antd';
import { useAddToCartMutation} from '../utils/userApi';
import { useAppSelector } from '../app/hooks';
import './Product.css';
import { Link } from 'react-router-dom';

interface Props {
    product: Product;

}
const style = { padding: '8px 0' };


// { product }: Props

export function Products() {

    const { data, isLoading, isError } = useGetAllProductsQuery()
    const [addToCart, addResult] = useAddToCartMutation();
    const user = useAppSelector(state => state.auth.user)




    if (isLoading) {
        return <p>Loading...</p>
    }
    if (isError) {
        return <p>Error...</p>
    }

    console.log("user on product page is", user)






    return (
        <div className="technology-wrap">
            <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }} align="middle" >
                .

                {data?.map(product =>
                    <>
                        {product?.images?.length ?

                            <div className="ant-col ant-col-md-24 ant-col-lg-8 divProduct  " style={style}  >


                                <Card className="technology-item" style={{ margin: 30, width: 350 }} title={product.name} bordered={true} >
                                    <Link to={'/product/' + product.id}>{/* sert à lister les détail d'un produit */}


                                        <Image
                                            width={300}
                                            src={product?.images[0].fileName?.startsWith('http') ? product?.images[0].fileName : `${process.env.REACT_APP_SERVER_URL}/uploads/${product?.images[0].fileName}`}

                                            className="imageClass"
                                            fallback={product.images[0].fileName}
                                        />




                                        <Button type="primary" onClick={() => addToCart(product.id!)}>Add to cart</Button>



                                        <Text>{product.price}€</Text>
                                    </Link>
                                </Card>
                            </div>




                            :

                            <div className="ant-col ant-col-md-24 ant-col-lg-8 divProduct  " style={style}  >


                                <Card className="technology-item" style={{ margin: 30, width: 350 }} title={product.name} bordered={true} >


                                    <Image
                                        width={300}
                                        src={"https://cdn.pixabay.com/photo/2017/01/25/18/07/mobile-homes-2008572_960_720.jpg"}

                                        className="imageClass"
                                    />




                                    <Button type="primary" onClick={() => addToCart(product.id!)}>Add to cart</Button>



                                    <Text>{product.price}€</Text>

                                </Card>
                            </div>
                        }</>

                )}
            </Row>
        </div>


    );


}