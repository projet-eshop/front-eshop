import 'antd/dist/antd.css';
import {  Card } from 'antd';
import { useGetAllOrdersQuery } from "../app/orderApi";

const { Meta } = Card;

const contentStyle = {
    height: '160px',
    color: '#fff',
    lineHeight: '160px',
    textAlign: 'center',
    background: '#364d79',
};

export function AdminUpdateOrder() {

    const { data, isLoading, isError } = useGetAllOrdersQuery();
    console.log("data order is", data)
    if (isLoading) {
        return <p>Loading...</p>
    }
    if (isError) {
        return <p>Error...</p>
    }

    return (

        <>
            {data?.map(orderData =>

            <Card
                hoverable
                style={{ width: 240 }}
            >
                <Meta title={orderData.rowProducts} description="www.instagram.com" />
                <Meta title={orderData.quantity} description="www.instagram.com" />
                <Meta title={orderData.priceTotal} description="www.instagram.com" />
                <Meta title={orderData.statusDelivered} description="www.instagram.com" />

                <Meta title={orderData.transport} description="www.instagram.com" />
                <Meta title={orderData.user?.name} description="www.instagram.com" />



            </Card>

)}
        </>

    );
}
