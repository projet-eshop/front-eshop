import { Card } from "antd";
import { Comment } from "../entities";

interface Props{
    comment: Comment
}

export function CommentAffichage({comment}:Props) {
    
    
    
    return(
        <>
            <Card style={{ width: 300 }}>
                {comment.description}
            </Card>
        </>
    )
}