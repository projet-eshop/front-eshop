import { Row, Col } from 'antd';
import { footer } from '../data.js';
import './foot.css'

export default function Foot() {
    return (
        <footer className="footer page-wrapper" style={{ position: "relative", bottom: "0" }}>
            <div className="footer-wrap page">
                <Row>
                    {
                        footer.map((foot, index) => (
                            <Col key={index.toString()} md={6} xs={24} className="footer-item-col">
                                <div className="footer-item">
                                    <h2>
                                        {foot.icon && <img style={{ marginRight: 16 }} src={foot.icon} alt="img" />}
                                        {foot.title}
                                    </h2>
                                    {foot.children.map(child => (
                                        <div key={child.link}>
                                            <a target="_blank " href={child.link}>
                                                {child.title}

                                                <span
                                                    style={{ color: 'rgba(255, 255, 255, 0.65)' }}
                                                > -
                                                </span>
                                            </a>
                                        </div>))}
                                </div>
                            </Col>
                        ))
                    }
                </Row>
            </div>
            <div className="footer-bottom">
                <div className="page">
                    <Row>
                        <Col md={4} xs={24} style={{ textAlign: 'left' }} className="mobile-hide">
                            <a
                                href="https://weibo.com/p/1005056420205486"
                                rel="noopener noreferrer"
                                target="_blank"
                                style={{ color: 'rgba(256, 256, 256, 0.9)', textAlign: 'left' }}
                            >
                                TinyTinyHouse Company
                            </a>
                        </Col>
                        <Col md={20} xs={24}>
                            <span
                                className="mobile-hide"
                                style={{ lineHeight: '16px', paddingRight: 12, marginRight: 11 }}
                            >
                                <a
                                    href="https://docs.alipay.com/policies/privacy/antfin"
                                    rel="noopener noreferrer"
                                    target="_blank"
                                >
                                    We seek future collaborations and partnerships
                                </a>
                            </span>

                            <span style={{ marginRight: 12 }}>Company number: 8724284hgf</span>
                            <span style={{ marginRight: 12 }}>Copyright © 2021 TINYTINYHOUSE</span>
                        </Col>
                    </Row>
                </div>

            </div>
        </footer>);
}
