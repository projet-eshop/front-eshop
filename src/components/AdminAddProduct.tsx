import React, { useState } from "react"
import 'antd/dist/antd.css';
import { Form, Input, InputNumber, Button, Select, UploadProps } from 'antd';
import { Upload } from 'antd';
import { Product } from '../entities';
import { UploadOutlined } from '@ant-design/icons';
import { UploadFile } from 'antd/lib/upload/interface';
import { usePostProductMutation } from '../app/AdminProductApi';

const layout = {
    labelCol: {
        span: 8,
    },
    wrapperCol: {
        span: 16,
    },
};
/* eslint-disable no-template-curly-in-string */

const validateMessages = {
    required: '${label} is required!',
    types: {
        email: '${label} is not a valid email!',
        number: '${label} is not a valid number!',
    },
    number: {
        range: '${label} must be between ${min} and ${max}',
    },
};

const { Option } = Select;

const suffixSelector = (
    <Form.Item name="suffix" noStyle>
        <Select
            style={{
                width: 70,
            }}
        >
            <Option value="USD">$</Option>
            <Option value="CNY">¥</Option>
        </Select>
    </Form.Item>
);
/* eslint-enable no-template-curly-in-string */

function getBase64(file: File) {
    return new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => resolve(reader.result);
        reader.onerror = error => reject(error);
    });
}

interface Props {
    values: Product;
}

interface targetType {
    target: HTMLInputElement;
}

interface eventType {
    onChangeAssignmentStatus: (
        selectType: string,
        value: React.ChangeEvent<HTMLSelectElement>
    ) => void;
}
interface IProps {
    handleSearchTyping(event: React.FormEvent<HTMLInputElement>): void;
}






export function AdminAddProduct (){

    /*     const [postProduct, { isLoading, data, error, isSuccess }] = usePostProductMutation();
     */
    const [name, setName] = useState('');
    const [description, setDescription] = useState('');
    const [price, setPrice] = useState<number | null>(0);
    const [images, setImages] = useState<string | null>('');

    const [addProduct, { isLoading }] = usePostProductMutation();
    


    const onFinish = async (values: any) => {
        console.log("the values sent are", values);
        const new_product = new FormData();
        new_product.append("price", values.price);
        new_product.append("description", values.description);
        new_product.append("name", values.name);
        new_product.append("file", values.images);

        await addProduct(values);
        
    };


    const [state, setState] = useState<{ fileList: UploadFile[] }>({
        fileList: []
    });


    const handleUpload = () => {
        const { fileList } = state;
        const formData = new FormData();
        fileList.forEach(image => {
            formData.append('files[]', image as any);
        });

    }

    const { fileList } = state;
    const props: UploadProps = {
        onRemove: image => {
            setState(state => {
                const index = state.fileList.indexOf(image as any);
                const newFileList = state.fileList.slice();
                newFileList.splice(index, 1);
                return {
                    fileList: newFileList,
                };
            });
        },
        beforeUpload: (file) => {
            setState(state => ({
                fileList: [...state.fileList, file],
            }));
            return false;
        },
        fileList,
    };


// const handleChangeNumber = (event: ChangeEvent<HTMLInputElement>) =>{
//     setPrice(event.currentTarget.value)
// }

// const handleChangePictures = (event: ChangeEvent<HTMLInputElement>) =>{
//     setImages(event.currentTarget.files)
// }






    return (
        <Form {...layout}  name="nest-messages" onFinish={onFinish} validateMessages={validateMessages}>
            <Form.Item
                name='name'
                label="Name"
                rules={[
                    {
                        required: true
                    },
                ]}
            >
                <Input onChange={event => setName(event.target.value)} />
            </Form.Item>


            <Form.Item
                name='description'
                tooltip="Description must be at least 20 characers?"

                label="Description"
                rules={[
                    {
                        required: true,
                        message: 'Please input a description!',
                        whitespace: true

                    },
                ]}>
                <Input.TextArea onChange={event => setDescription(event.target.value)} />
            </Form.Item>

            <Form.Item
                name="price"
                label="Price"
                rules={[
                    {
                        required: true,
                        message: 'Please input price amount!',
                    },
                ]}
            >
                <InputNumber

                    // onChange = {(e: React.FormEvent<HTMLInputElement>) => {
                    //     const newValue = e.currentTarget.value;
                    // }}

                    // onChange = {(e: React.ChangeEvent<HTMLInputElement>)=> {
                    //     const newValue = e.target.value;
                    //  }}
                    // prefix={suffixSelector}
                    style={{
                        width: '100%',
                    }}
                />
            </Form.Item>

            <Form.Item
                name="image"
                label="image"
            >
                <Upload {...props} 
                // onChange={handleChangePictures}
                >
                    <Button icon={<UploadOutlined />}>Select File</Button>
                </Upload>

            </Form.Item>

            <Form.Item wrapperCol={{ ...layout.wrapperCol, offset: 8 }}>
                <Button type="primary" htmlType="submit" onClick={handleUpload} disabled={fileList.length === 0}>
                    Submit
                </Button>
            </Form.Item>
        </Form>
    );
}
