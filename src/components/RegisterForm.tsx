import { Form, Input, Button, DatePicker, Card } from 'antd';
import { Moment } from 'moment';
import { useUserRegisterMutation } from '../utils/userApi';
import './Form.css'

const cardItemLayout = {
    labelCol: {
        xs: { span: 24 },
        sm: { span: 8 },
    },
    wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
    },
};
const formItemLayout = {
    labelCol: {
        xs: { span: 24 },
        sm: { span: 5 },
    },
    wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
    },
}



export const RegistrationForm = () => {
    const [form] = Form.useForm();

    const [registerPost, { isLoading }] = useUserRegisterMutation();

    const onFinish = (values: any) => {
        registerPost({ ...values, birthdate: values.birthdate?.format('YYYY-MM-DD') })
    }


    function onChange(date: Moment | null, dateString: string) {
        console.log(date);
        date?.format('YYYY-MM-DD')
        console.log('date is', date?.format('YYYY-MM-DD'))

    }


    return (
<div className="loginFormContainer">
        <Card {...cardItemLayout} >
            <Form {...formItemLayout}
                form={form}
                name="register"
                onFinish={onFinish}
                initialValues={{
                    prefix: '+33',
                }}
                scrollToFirstError
            >
                <Form.Item
                    name="email"
                    label="E-mail"
                    rules={[
                        {
                            type: 'email',
                            message: 'The input is not valid E-mail!',
                        },
                        {
                            required: true,
                            message: 'Please input your E-mail!',
                        },
                    ]}
                >
                    <Input />
                </Form.Item>

                <Form.Item
                    name="name"
                    label="Name"
                    tooltip="Please input your Name"
                    rules={[{ required: true, message: 'Please input your Name!', whitespace: true }]}
                >
                    <Input />
                </Form.Item>

                <Form.Item
                    name="password"
                    label="Password"
                    rules={[
                        {
                            required: true,
                            message: 'Please input your password!',
                        }, {
                            min: 8,
                            message: 'Password must have a minimum length of 8'
                        },
                    ]}
                    hasFeedback
                >
                    <Input.Password />
                </Form.Item>

                <Form.Item
                    name="firstName"
                    label="firstname"
                    rules={[{ required: true, message: 'Please input your first-name!', whitespace: true }]}
                >
                    <Input />
                </Form.Item>

                <Form.Item
                    name="birthdate"
                    label="Birthdate"
                    rules={[{ required: true, message: 'Please input your birthdate' }]}
                >
                    <DatePicker onChange={onChange} />
                </Form.Item>

                <Form.Item
                    name="address"
                    label="Adresse"
                    rules={[{ required: true, message: 'Please input your adresse' }]}
                >
                    <Input style={{ width: '100%' }} />
                </Form.Item>

                <div className='button-form'>
                    <Button className='btn-form' type="primary" htmlType="submit">
                        Register
                    </Button>
                </div>
            </Form>
        </Card>
        </div>
    );
};


