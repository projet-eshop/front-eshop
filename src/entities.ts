export interface Product {
    id?: number;
    name?: string;
    description?: string;
    price?: string;
    amount?:number;
    comments?: Comment[];
    rowProducts?: RowProduct[];
    images?: Image[];

} 

export interface Image {
    id?: number;
    fileName?: string;
    product?: Product;
    url?:string
    thumbnail?:string
}

export interface RowProduct {
    id?: number;
    price?: number;
    quantity?: number;
    order?: Order
    product?: Product[];
}

export interface Order {
    id?: number;
    dateCrea?: Date;
    quantity?: number;
    statusDelivered?: number;
    priceTotal?: number;
    user?: User;
    transport?: Transport;
    rowProducts?: RowProduct[];
}

export interface User {
    id?: number;
    name?: string;
    firstName?: string;
    password?: string;
    email?: string;
    role?: string;
    birthdate?: Date;
    address: string;
    comments?: Comment[];
    shoppingCart?: RowProduct[];
    orders: Order[]
}



export interface Transport {
    id?: number;
    name?: string;
    pricePerWeight?: number;
    orders?: Order[];
}

export interface Comment {
    id?: number;
    ratingNumber?: number;
    description?: string;
    date?: Date;
    user?:User;
    product?:Product;
}

