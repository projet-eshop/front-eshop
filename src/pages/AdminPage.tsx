import { useGetAllProductsQuery} from "../app/AdminProductApi";
import { AdminProduct } from "../components/AdminProduct";
import { Product } from '../entities';

import React from 'react';
import ReactDOM from 'react-dom';
import 'antd/dist/antd.css';
import { Tabs } from 'antd';
import { AdminAddProduct } from "../components/AdminAddProduct";
import { AdminUpdateOrder } from "../components/AdminUpdateOrder";
import './Admin.css'

const { TabPane } = Tabs;


export function AdminPage() {
    const { data, isLoading, isError } = useGetAllProductsQuery();


    function callback(key: string) {
        console.log(key);
    }


    if (isLoading) {
        return <p>Loading...</p>
    }
    if (isError) {
        return <p>Error...</p>
    }

    return (
        <div className="container">

            <Tabs defaultActiveKey="1" onChange={callback}>
                <TabPane tab="View a product in detail" key="1">
                    View product in detail
                    {data?.map((item: Product) => 
                    <div key={item.id}>
                        <AdminProduct product={item} />
                        </div>)}

                </TabPane>
                <TabPane tab="Add a product" key="2">
                    Add a product
                    <AdminAddProduct />
                </TabPane>
                <TabPane tab="View orders" key="3">
                    View orders
                    <AdminUpdateOrder />
                </TabPane>
            </Tabs>
            
        </div>
    )

}