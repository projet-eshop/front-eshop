import { useEffect, useState } from 'react';
import { Drawer, Button } from 'antd';
import { Progress } from 'antd';
import { ShoppingOutlined } from '@ant-design/icons';
import { Row, Col } from 'antd';
import { Badge } from 'antd';
import { RowProduct } from '../entities';
import { useAppSelector } from '../app/hooks';
import { useGetAllProductsQuery } from '../app/AdminProductApi';
import { usePostOrderMutation } from '../app/orderApi';
import { useHistory } from 'react-router';

export function Cart() {

    const user = useAppSelector(state => state.auth.user);
    console.log("user on cart page is", user)
    console.log('shoppin cart on cart page is', user?.shoppingCart);
    let history = useHistory();



    const { isLoading, data, error } = useGetAllProductsQuery();//shoppincartRoute


    const [cartOpen, setCartOpen] = useState(false);
    const [addOrder, setAddOrder] = usePostOrderMutation();
    const [quantity, setQuantity] = useState<number | null>(0);
    const [status, setStatus] = useState<number | null>(0);
    const [priceTotal, setPriceTotal] = useState<number | null>(0);


    // const clearCart = () =>cartOpen([]);

    const onFinish = async (rowProducts: any) => {
        rowProducts = user?.shoppingCart;
        console.log("the rowProducts sent are", rowProducts);

        await addOrder(rowProducts);

        user?.shoppingCart?.slice(0, user?.shoppingCart?.length);
        history.push(`/user`)

    };

    useEffect(() => {
        const userUdpate = user;
    }, []);


    const [cartItems, setCartItems] = useState([] as RowProduct[]);


    console.log(data);

    const getTotalItems = (items: RowProduct[]) =>
        items.reduce((ack: number, item: any) => ack + item.quantity, 0);

    const handleAddToCart = (clickedItem: RowProduct) => {
        setCartItems(prev => {
            const isItemInCart = prev.find(item => item.id === clickedItem.id);

            if (isItemInCart) {
                return prev.map(item =>
                    item.id === clickedItem.id
                        ? { ...item, quantity: item?.quantity! + 1 }
                        : item
                );
            }
            return [...prev, { ...clickedItem, quantity: 1 }];
        });
    };

    const handleRemoveFromCart = (id: number) => {
        setCartItems(prev =>
            prev.reduce((ack, item) => {
                if (item.id === id) {
                    if (item.quantity === 1) return ack;
                    return [...ack, { ...item, amount: item?.quantity! - 1 }];
                } else {
                    return [...ack, item];
                }
            }, [] as RowProduct[])
        );
    };

    if (isLoading) return <Progress />;
    if (error) return <div>Something went wrong ...</div>;

    const showDrawer = () => {
        setCartOpen(true);
    };
    const onClose = () => {
        setCartOpen(false);
    };



    return (
        <div>
            <Drawer placement="right" onClose={() => setCartOpen(false)} visible={cartOpen} >
                <Row>
                    {user?.shoppingCart?.map(item => (
                        <Col key={item.id} span={8}>
                            {item === 0 ? <p>No items in cart.</p> : null}
                            <p>Price: {item.price}</p>
                            <p>Quantity: {item.quantity}</p>
                            <p>Id : {item.id}</p>
                            <p>Order :{item.order}</p>

                            <h2>Total for {item.id}: ${(item?.quantity! * item?.price!).toFixed(2)}</h2>
                            {/* <Products product={data} handleAddToCart={handleAddToCart} /> */}
                        </Col>
                    ))}

                    <h1> Total for all is: {user?.shoppingCart?.reduce((total: number, item) => total + item?.quantity! * item?.price!, 0)}</h1>
                    <Button type="primary" onClick={onFinish} >Order</Button>

                </Row>
            </Drawer>
            <Button onClick={() => setCartOpen(true)}>
                <Badge count={user?.shoppingCart?.length} >
                    <ShoppingOutlined />
                </Badge>
            </Button>


        </div>

    );
};

export default Cart;
