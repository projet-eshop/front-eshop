import { useGetThisUserQuery } from "../utils/userApi";
import { Card } from 'antd';
import './userpage.css'

const { Meta } = Card;

export function UserPage() {
    const { data } = useGetThisUserQuery();
    // console.log("orders data re ", data?.orders.map(item => item.quantity))

    return (
        <div className="userPage">

            <Card
                hoverable
                style={{ width: 240 }}
            >
                <Meta title={data?.email} />
                <Meta title={data?.name} />

                <Meta title={data?.firstName} />

                <Meta title={data?.orders} />

            </Card>
        </div>
    )
}