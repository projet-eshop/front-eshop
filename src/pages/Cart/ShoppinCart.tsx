import { RowProduct } from '../../entities';
import { useAppSelector } from '../../app/hooks';

type Props = {
  cartItems: RowProduct[];
  addToCart: (clickedItem: RowProduct) => void;
  removeFromCart: (id: number) => void;

};

export function ShoppingCart() {


  const user = useAppSelector(state => state.auth.user);
  console.log("user shop cart is", user)

  return (
    <div>
      <h2>Your Shopping Cart</h2>

      <div>
        {user?.shoppingCart?.map(item => (
          <div>

          </div>
        ))}
      </div>

    </div>
  );
}

export default ShoppingCart;