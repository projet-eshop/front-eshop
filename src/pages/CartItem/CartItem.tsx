import { useAppSelector } from '../../app/hooks';

export function CartItem() {


  const user = useAppSelector(state => state.auth.user);
  console.log("user shop cart is", user)

  return (

    <div>
      {user?.shoppingCart?.map(item => (
        <div key={item.id} >

          <p>Price: {item.price}</p>
          <p>Quantity: {item.quantity}</p>
          <p>Id : {item.id}</p>
          <p>Order :{item.order}</p>
          <p>Price: ${item?.price}</p>
          <p>Total: ${(item?.quantity! * item?.price!).toFixed(2)}</p>


          <div className='buttons'>
          </div>
        </div>
      ))}
    </div>

  );
}

export default CartItem;