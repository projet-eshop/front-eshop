import { Divider, Row } from "antd";
import { useGetAllProductsQuery } from "../app/AdminProductApi";
import { CarouselComponent } from "../components/Carousel";
import { Products } from "../components/Products";
import { useAppSelector } from "../app/hooks";
import './Home.css'
import Banner from "../components/Banner";


export function Home() {
    // const { data, isLoading, isError } = useGetAllOrdersQuery();

    const { data } = useGetAllProductsQuery()

    const user = useAppSelector(state => state.auth.user)
    console.log("user on home is", user)

    return (
        <div style={{ margin: "auto" }} >
            <CarouselComponent />

            <Divider />
            <Banner />

            <Row justify='space-around'>
                <Products />
            </Row>
        </div>
    )
}
