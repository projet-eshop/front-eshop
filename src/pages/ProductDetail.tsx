import { Button, Col, Row } from "antd";
import { useParams } from "react-router-dom";
import { useGetOneProductQuery } from "../app/productApi";
import { RouterParams } from "../App";
import { Image } from "antd";
import Meta from "antd/lib/card/Meta";
import { StarOutlined } from "@ant-design/icons";
import { Space } from "antd";
import { useAppSelector } from "../app/hooks";
import { TextAreaComment } from "../components/addComment";
import { CommentAffichage } from "../components/commentAffichage";

export function ProductDetail() {
  const { id } = useParams<RouterParams>();
  const user = useAppSelector(state => state.auth.user)
console.log(id);


  const { data, isLoading, isError, refetch } = useGetOneProductQuery(Number(id));

  function affichEtoile(nbEtoile?: number) {
    if (nbEtoile) {
      for (let i = 1; i <= nbEtoile; i++) {
        return <StarOutlined />;
      }
    }
  }

  if (isError) {
    return <p>error</p>;
  }
  if (isLoading) {
    return <p>loading</p>;
  }
  if (data && data.images) {
    return (
      <>
        <Row>
          <Col span={8}>
            {
              <img
                alt="product"
                src={
                  `${process.env.REACT_APP_SERVER_URL}${data.images[0].url}`
                }
              />
            }
          </Col>
          <Col span={8}>{data.name}</Col>
        </Row>

        <Meta title={data.name} description={data.description} />
        <Row>
          {data.comments &&
            data.comments.map((item) => (
              <Col>
                <p> {affichEtoile(item.ratingNumber)}</p>
                {/* <p>{item.description}</p> */}
              </Col>
            ))}
        </Row>
        <Space size={[8, 16]} wrap>
          {data.images.map((item) => (
            <Image
              width={200}
              src={
                item.fileName?.startsWith("http")
                  ? item.fileName
                  : `${process.env.REACT_APP_SERVER_URL}/uploads/${item.fileName}`
              }
            />
          ))}
        </Space>
        <Button type="primary" shape="circle">
          +
        </Button>

        <TextAreaComment refetch={refetch} id={data.id!} />
              {data.comments && data.comments.map(item =>
              <CommentAffichage key={item.id} comment={item}/>
              )}
        
      </>
    );
  }
  return <div>Not found</div>;
}