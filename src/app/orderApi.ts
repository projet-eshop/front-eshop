import {createApi, fetchBaseQuery} from '@reduxjs/toolkit/query/react';
import { Order } from '../entities';
import { prepare } from '../utils/token';

export const orderApi = createApi({
    reducerPath: 'orderApi',
    tagTypes: ['OrderList'],
    baseQuery: fetchBaseQuery({baseUrl: process.env.REACT_APP_SERVER_URL +'/api/order', prepareHeaders: prepare}),
    endpoints: (builder) => ({
        getAllOrders: builder.query<Order[], void>({
            query: () =>  '/',
            providesTags: ['OrderList']
        }),
        getOneOrder: builder.query<Order, number>({
            query: (id) => '/'+id
        }),
        postOrder: builder.mutation<Order, Order>({
            query: (body) => ({
                url: '/',
                method: 'POST',
                body
            }),
            invalidatesTags: ['OrderList']
        }),
        deleteOrder: builder.mutation<void , number >({
            query: (id)=>({
                url:'/'+id,
                method: 'DELETE'
            }),
            invalidatesTags: ['OrderList']
        })
    })
});


export const {useGetAllOrdersQuery, usePostOrderMutation, useGetOneOrderQuery, useDeleteOrderMutation} = orderApi;
