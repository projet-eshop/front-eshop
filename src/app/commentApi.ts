import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/dist/query/react";
import { Comment } from "../entities";
import { prepare } from "../utils/token";

export const commentApi = createApi({
    reducerPath: 'commentApi',
    tagTypes: ['Comments'],
    baseQuery: fetchBaseQuery({baseUrl: process.env.REACT_APP_SERVER_URL + '/api/comment', prepareHeaders:prepare}),
    endpoints: (builder) => ({
        getOneComment: builder.query<Comment, number>({
            query: (id) => '/' + id
        }),
        postComment: builder.mutation<Comment, {body: Comment, id:number}>({
            query: (param) => ({
                url: '/'+param.id,
                method: 'POST',
                body:param.body
            }),
            invalidatesTags:['Comments']
        })
    })
})

export const {useGetOneCommentQuery, usePostCommentMutation} = commentApi;