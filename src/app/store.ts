import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit';
import { userApi } from '../utils/userApi';
import { AdminProductApi } from './AdminProductApi'
import authSlice from './authSlice';
import { commentApi } from './commentApi';
import { orderApi } from './orderApi';
import { productApi } from './productApi';



export const store = configureStore({
    reducer: {
        [AdminProductApi.reducerPath]: AdminProductApi.reducer,
        [userApi.reducerPath]: userApi.reducer,
        [orderApi.reducerPath]: orderApi.reducer,
        [productApi.reducerPath]:productApi.reducer,
        auth: authSlice,

        [orderApi.reducerPath]: orderApi.reducer,
        [commentApi.reducerPath]: commentApi.reducer
    },
    middleware: (getDefaultMiddleware) =>
        getDefaultMiddleware().concat(AdminProductApi.middleware, orderApi.middleware, userApi.middleware, productApi.middleware, commentApi.middleware),
});


export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
    ReturnType,
    RootState,
    unknown,
    Action<string>
>
