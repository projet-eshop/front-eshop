import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/dist/query/react";
import { Product } from "../entities";
import { prepare } from "../utils/token";


export const productApi = createApi({
    reducerPath: 'productApi',
    tagTypes: ['Products', 'oneProduct'],
    baseQuery: fetchBaseQuery({ baseUrl: process.env.REACT_APP_SERVER_URL + '/api/product' }),
    endpoints: (builder) => ({
        getAllProduct: builder.query<Product, void>({
            query: () => '/',
            providesTags: ['Products']
        }),

        getOneProduct: builder.query<Product, number>({
            query: (id) => '/' + id,
            providesTags:['oneProduct']
        }),

        postProduct: builder.mutation<Product, Product>({
            query: (body) => ({
                url: '/',
                method: 'POST',
                body
            }),
            invalidatesTags: ['Products']
        })
    })
});

export const {useGetAllProductQuery, useGetOneProductQuery, usePostProductMutation} = productApi;