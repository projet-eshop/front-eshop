import {createApi, fetchBaseQuery} from '@reduxjs/toolkit/query/react';
import { Product } from '../entities';
import { prepare } from '../utils/token';


export const AdminProductApi = createApi({
    reducerPath: 'AdminProductApi',
    tagTypes: ['AdminProduct'],
    baseQuery: fetchBaseQuery({baseUrl: process.env.REACT_APP_SERVER_URL +'/api/product', prepareHeaders: prepare}),
    endpoints: (builder) => ({
        getAllProducts: builder.query<Product[], void>({
            query: () =>  '/',
            providesTags: ['AdminProduct']
        }),
        patchProduct: builder.mutation<Product, Product>({
            query: (id) => ({
                url: '/'+id,
                method: 'PATCH',
                id
            }),
            invalidatesTags: ['AdminProduct']
        }),
        deleteProduct: builder.mutation<Product, Product>({
            query: (id) => ({
                url: '/'+id,
                method: 'DELETE',
                id
            }),
            invalidatesTags: ['AdminProduct']
        }),
        postProduct: builder.mutation<Product, Product>({
            query: (body) => ({
                url: '/',
                method: 'POST',
                body
            }),
            invalidatesTags: ['AdminProduct']
        }),
        
    })
});


export const {useGetAllProductsQuery, usePatchProductMutation,useDeleteProductMutation, usePostProductMutation} = AdminProductApi;